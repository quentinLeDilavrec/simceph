\documentclass[conference]{IEEEtran}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{url}

\newenvironment{todo}{\color{red}}{}

\begin{document}

	\title{Survey of Software Interception Techniques in the Context of Cloud Applications}
	\author{%
		\IEEEauthorblockN{Joseph Cabrita}
		\IEEEauthorblockA{Univ Rennes\\F-35000, France}\and
		\IEEEauthorblockN{Clément Courageux-Sudan}
		\IEEEauthorblockA{Univ Rennes\\F-35000, France}\and
		\IEEEauthorblockN{Jean-Michel Gorius}
		\IEEEauthorblockA{Univ Rennes\\F-35000, France}\and
		\IEEEauthorblockN{Quentin Le~Dilavrec}
		\IEEEauthorblockA{Univ Rennes\\F-35000, France}
	}

	\maketitle

	\begin{abstract}
		To understand their behavior, complex computational systems need be carefully studied. Emulation is one way of achieving this goal. In this paper, we give a broad overview of tools and techniques which could be used to emulate cloud-based applications. We focus on software interception techniques, i.e. methods acting based on events happening during application execution, from a low-level to a high-level point of view. In the future, we plan to combine such interception methods with an existing simulation platform like SimGrid to study complex applications in depth.
	\end{abstract}

	\section{Introduction}
	\label{sec:introduction}

		Cloud infrastructures have become ubiquitous, as they are used to provide numerous server-based services. From websites to scientific computing, clouds abstract the user away from the underlying hardware and software peculiarities. A typical cloud infrastructure is made out of a large number of service layers. Those layers interact in a complex manner. The complexity underlying the operations of such systems are hard to grasp precisely by using only mathematical models. Moreover, the development of such models is a time consuming and error-prone task.

		Studying such complex applications is tedious. There is a need for adapted tools designed to abstract the user away from their underlying complexity. To overcome the difficulties implied by the intricate behavior of cloud-based applications, one can resort to software interception.

		Software interception techniques enable the user to capture runtime events and react to them according to a given set of rules. There are different possible levels of interception, each with its own limitations and opportunities. The more fine-grained interception methods capture each and every event and notify the user, while more coarse-grained techniques capture global behaviors. In the first case, the user can react to very low-level events, but when faced with a complex applications, the event-processing infrastructure can quickly become flooded. On the other hand, one can control the semantic level at which to work by adjusting the level at which to intercept events.

		In this paper, we present a broad overview of software interception techniques applicable to the study of cloud applications. This paper is organized as follows. Section~\ref{sec:context} gives a description of the context of our study, putting emphasize on potential solutions for the study of cloud-based applications and describing software emulation. Section~\ref{sec:stateoftheart} presents various software interception techniques usable for cloud application study. It follows a low-to-high-level approach. Eventually, section~\ref{sec:conclusion} concludes this paper and presents our future work.

	\section{Context}
	\label{sec:context}

		This section introduces methods which can be used to study the behavior of complex cloud-based applications (section~\ref{subsec:behavior}). It focuses especially on software execution environment emulation (section~\ref{subsec:emulation}). The last part (section~\ref{subsec:aim}) gives an overview of the problem we aim at solving in a planned future work.

		\subsection{Behavioral study of cloud applications}
		\label{subsec:behavior}

			The study of the behavior and evolution of complex distributed and layered cloud systems can follow three main approaches~\cite{gustedt}: real platform execution, simulation and emulation.

			In the first case, the application is executed on an existing cloud infrastructure and its behavior is studied under real execution conditions. This approach needs a large computing infrastructure to execute even the simplest of tests. Moreover, the diversity and heterogeneity of hardware used in clouds is difficult to model in the case of production platforms. Existing execution platforms such as IGridA or IDRIS suffer from numerous drawbacks. Experiments led on those infrastructures are difficult to reproduce, as they are subject to external noise caused by applications running concurrently on the same platform. A thorough understanding of an application's execution can sometimes be hard even on specialized experimental platforms such as GRID'5000~\cite{cappello}.

			The second possible approach to cloud-based application study, namely simulation, makes use of an application model plugged into a simulator. To simulate cloud applications, one must first abstract the application and its execution environment and therefore create an execution model. The synthesis of this model relies on the identification of some application and environment properties which are transformed by using a mathematical formulation. In the end, the simulator executes the application's model instead of the real application. A simulator like SimGrid~\cite{casanova} is capable of handling models involving hundreds of computation nodes. Simulation makes experiments highly reproducible but its major drawback is the inability to capture the entire and exact behavior of an application.

			The last possible technique which can be used to study a cloud-based application's behavior is emulation. The emulated application is executed in a virtualized environment provided by an \emph{emulator}. The emulator intercepts particular application-emitted messages and/or signals at a given abstraction level. It uses them to emulate the effect of execution on the application's environment. Emulation aims at overcoming the limitations of simulation and real execution. More precisely, the code of the application is actually run on a host machine but, thanks to the virtualization process, the application is tricked into believing it is executing on a different host. Emulation can take place after a real execution, i.e. \emph{off-line}, by replaying saved traces of actions taken by the emulated application. It can also happen \emph{on-line}. In this case, the application is executed and each time an action is intercepted by the emulator, the application is paused. The emulator computes the result of the intercepted action on the emulated platform and then resumes normal execution. Emulation makes it possible to run applications designed to be run on multiple hosts at once on a single machine. This eases the observation and analysis of complex programs, as experiments can be run on a personal computer.

		\subsection{Software emulation}
		\label{subsec:emulation}

			In the following, we consider cloud-based application study using the standpoint of emulation. This approach easily overcomes the difficulties implied by heterogeneous execution infrastructures. Such infrastructures are similar to those typically found in cloud software-and hardware stacks. Emulation abstracts away the need for a large-scale real execution infrastructure.

			Emulation can either be on-line of off-line. One of the greatest benefits of off-line emulation is the ability to post-process execution traces obtained after executing an application on real hardware before sending them to the emulator. However, the off-line approach uses a lot more memory to store all of the event logs and traces captured during execution. Those can quickly become intractable in a large and complex execution environment. Moreover, off-line emulation is impractical in the case of dynamic applications, i.e. applications whose execution path is dependent on the state of the environment. In the case of on-line emulation, on the other hand, there is no need to record each event occurring during execution, as each one of them is immediately processed by the emulator. On-line emulation also has the advantage of enabling the user to interact with the emulated application in real time, changing experimental conditions on the fly.

			One can use an on-line emulator to virtualize a cloud-based application's execution environment and study its behavior and its evolution. A typical on-line emulator uses a variety of \emph{hooks} inserted around the application's code to capture numerous events happening during execution. These hooks can be inserted at different levels in the software stack and can take numerous forms. The interception methods discussed in the following sections can be used to build up such hooks.

		\subsection{Problem specification}
		\label{subsec:aim}

			Our work aims at presenting a proof of concept showing that it is possible to apply software interception techniques to emulate cloud-based applications. We focus our efforts on emulating Ceph.

			Ceph is a widely used cloud-based application providing storage services which are able to handle very large amounts of data~\cite{weil}. It is an open source project with an extensive documentation. The open access to its source code makes it a great candidate for source level interception, as discussed in section~\ref{subsec:sourcelevel}.

			To emulate Ceph, we choose to use SimGrid~\cite{casanova}, and particularly its remote command execution API, \emph{Remote SimGrid} (RSG). SimGrid is a scientific instrument used to study the behavior of large-scale distributed systems such as compute-grids, clouds, HPC or P2P (\emph{peer to peer}) systems. It can be used to evaluate heuristics, prototype applications or even assess legacy MPI (\emph{Message Passing Interface}) applications. Due to the lack of a reliable interception mechanism, cloud-based application study using SimGrid is currently limited to simulation. As a consequence, the end-user has to model the execution environment of each application so as to approximate its real-world observed behavior. We aim at overcoming this limitation by introducing a new way to study cloud-based applications using RSG.

			We study numerous possible software interception techniques which could allow us to pass runtime events to SimGrid using RSG. Those techniques exhibit various levels of granularity and can be implemented at different layers in the software and/or hardware stack.

	\section{State of the Art}
	\label{sec:stateoftheart}

		Software interception is a set of means by which an external program can capture events happening during the execution of another application. This kind of capture can be used to monitor or profile applications. It can also be used to alter the behavior of the execution environment of an executing application. The latter scenario is the one we discuss in the rest of this paper. We particularly focus on software interception of network communications, which are essential to every cloud-based application. Each of the following sections discusses a particular level of interception, i.e. a layer in the software or hardware stack at which events are captured, from a low-level to a high-level standpoint.

		\subsection{Network-level interception}
		\label{subsec:networklevel}

			The lowest-level approach to intercept network communication events is the one located directly at the network hardware layer. Using a hardware-level virtual machine, one can trick a running application into using a filtered virtual Ethernet port for all of its network communication operations.

			This approach is one of many taken by ns-3, a discrete-event network simulator for Internet systems~\cite{henderson}. This widely used simulator makes use of a \texttt{qemu} virtual machine (VM) to execute an application. This VM sets up a virtual Ethernet port through which every network communication issued by the executed application is established. This port is in turn connected to a physical Ethernet port through a network bridge. This configuration allows an external network packet analysis tool such as \textit{Wireshark}~\cite{combs} to intercept the communication packets issued by the application under study. It can then notify other systems of captured communication events and even get the exact content of each message. This interception method is completely transparent to the underlying VM and application, as every behavioral observation is made entirely outside of the execution environment. A similar result can be achieved by setting up a virtual network on the host machine executing the application.

			A major drawback to this interception technique is its extremely fine-grained event capture. By intercepting every network packet emitted by the executed application through the VM's virtual Ethernet port, one has a very local view of this application's behavior and actions. In fact, even initiating a simple network communication can take several dozens of packets. One cannot focus his attention at such fine level of detail. A potential solution to this difficulty is to go up one layer of abstraction and intercept events at the driver level.

		\subsection{Driver-level interception}
		\label{subsec:driverlevel}

			Located just above the raw network layer, drivers are the means used by operating systems to communicate with the network hardware, typically a network card on the host machine. It is possible to intercept network communication events on the driver level by using the hardware abstraction layer provided by an operating system (OS) or a \emph{type~1 hypervisor}.

			An hypervisor or \emph{virtual machine monitor} is a program designed to create, run and manage a set of virtual machines. There are two types of hypervisors, namely \emph{type~1} and \emph{type~2} hypervisors. Type~1 hypervisors run directly on the hardware of the host machine and act as a kind of operating system. They manage system calls and interact directly with the hardware using drivers. Type~2 hypervisors, or hosted hypervisors, run on a host OS. They forward system calls to the underlying OS, which in turn is in charge of interacting with the hardware.

			By writing a custom network driver and injecting it in a type~1 hypervisor's hardware interaction layer, one can use it to notify an external program of every network event. This in turn can be used to emulate an execution environment and propagate the effects of the intercepted network operations on it. However, this approach is hard to implement in practice, as one has to write a low-level network driver in order to achieve successful interception. This driver-based method also has a fine interception granularity, as it notifies the emulator of each network transaction initiated with the network card. As is the case for network-level interception, this level of detail is too high in comparison of the execution time and operational complexity of cloud systems. In the next section, we go further up in the interception layer hierarchy and focus on kernel-level interception.

		\subsection{Kernel-level interception}
		\label{subsec:kernellevel}

			The kernel is a program located at the heart of an OS. It is the one responsible for managing every possible interactions between programs and/or hardware. Intercepting events at the kernel level can be achieved by dynamically interacting with the kernel. The main approach used to achieve such behavior is the use of a \emph{libOS}. A libOS is an operating system kernel (e.g. a given Linux kernel) packaged as a dynamic library. This dynamic library can be used by applications to interact directly with kernel-level functionalities.

			A first approach used to intercept network communication events at the kernel level using a libOS-based solution is presented by Zhang~et~al.~\cite{kylinx} in the form of the KylinX project. Their goal is to sandbox an application to avoid malicious behavior on cloud infrastructures. They use virtual machines managed in a process-like fashion by the server's hypervisor to control the behavior of each and every running application. This method introduces the concept of \emph{process-like VMs} (pVMs) and treats the hypervisor as a low-level operating system. Those pVMs run a \emph{unikernel}, which in turn is responsible for executing a given application. Unikernels are operating systems in which each running program shares the same address space as the other programs running at the same time. Each process-like VM loads the unikernel as a shared library, using the libOS pattern, and intercepts system calls at the kernel level. The intercepted calls are then filtered and transmitted to the underlying hypervisor if needed.

			Another example of the use of a libOS to achieve interception of an application's events is DCE~\cite{tazaki}. DCE, which stands for ``Direct Code Execution'', aims at executing unmodified applications within the ns-3 simulator mentioned in section~\ref{subsec:networklevel}. To achieve that goal, the network stack implementation of the Linux kernel is copied and modified at the lowest level to interact with the ns-3 simulator. This code is embedded into DCE as a libOS. This library can then be used by the simulated application to communicate with the simulator instead of the real network. The simulated application doesn't have to be modified because it uses the classical implementations of sockets found in the kernel. This makes it possible for an application to communicate transparently with the ns-3 simulator.

			The level of detail offered by kernel-level interception offers more flexibility in the context of application emulation, as it gives the emulator a bit more semantic information about the events captured during execution. For example, the interception of the \texttt{open} system call informs the emulator that the application under study wishes to open a file descriptor. However, this information is often not usable, as system calls are ubiquitous when calling higher-level functions. It can be useful to go up a level in the software interception hierarchy and intercept events at the binary-level to ease the interpretation of captured events.

		\subsection{Binary-level interception}
		\label{subsec:binarylevel}

			Executable binaries offer a higher level of abstraction when compared to a kernel. There exist numerous ways of intercepting events at the binary level, or at the library level in the case of a library dynamically loaded by a process. Those include libOSes, \texttt{LD\_PRELOAD}, the \texttt{ptrace} system call or a dynamic call capture method implemented in the DCE~\cite{tazaki} project. In the next paragraphs, we describe some projects which make use of those techniques to intercept execution events.

			\subsubsection{Dynamic library pre-loading}
			\label{subsubsec:dynamicpreload}

				One of the main techniques used to intercept events at the process level is library call interception. On a Linux host, this can be achieved by using the \texttt{LD\_PRELOAD} environment variable. \texttt{LD\_PRELOAD} is a variable that can be used to specify shared libraries that will be loaded with a higher priority at runtime. When launching an application, the dynamic linker loads the libraries specified by \texttt{LD\_PRELOAD} before any other. As loaded symbols are remembered by order of first load, this technique enables a user to effectively replace functions by others dynamically. This feature can easily be used to perform interception by overwriting a function used by an application.

				As an example, we can consider the POSIX function \texttt{getimeofday} which returns the system time. One could make his own implementation of the function, which would return a simulated time instead of the real time, and embed it into a shared library. \texttt{LD\_PRELOAD} can then be set to use this new library before running a program that uses this function. This has the effect of redirecting every call to \texttt{gettimeofday} when running the program to the new implementation.

				One project using this library pre-loading trick is \emph{MicroGrid}~\cite{song}. Microgrid is a grid emulator used to analyze the behaviour of real grid applications inside virtual environments. To achieve that, Microgrid makes use of \texttt{LD\_PRELOAD} to intercept some function calls through a custom made library, namely \texttt{libmgrid}. Two different resources are intercepted with this library. In the first place, calls to network services (e.g. \texttt{send} or \texttt{receive}) are redirected to a network simulator that reproduces the virtual environment. Then, functions related to the system's time are intercepted to return the time of the virtual environment. The latter is calculated by comparing the time needed to perform actions on the host system to the computational power of the virtual system.

			\subsubsection{Process-tracing system call}
			\label{subsubsec:ptrace}

				Dynamic library pre-loading is not the only way one can intercept events during a process' execution. In fact, the Linux kernel provides a system call named \texttt{ptrace}, which allows a program to interact with another using \emph{signals}. Signals are similar to commands emitted and received by and from programs. They allow one program to change the behavior of another one by using a very simple communication scheme. The \texttt{ptrace} system call makes use of such signals to manipulate the execution behavior of a program called the \emph{tracee} based on given events, for example the call of a given library function or system function. Using \texttt{ptrace}, one can intercept this call and, for instance, insert code inside the currently executed code section before resuming normal execution. The inserted code will then eventually be executed as if it were part of the original application.

				The \texttt{ptrace} system call can sometimes be difficult to deal with. To ease the filtering and modification of system calls issued by a running application, one can use a \texttt{ptrace} extension called \texttt{seccomp-bpf} (\texttt{seccomp} Berkeley Packet Filtering). This extension is based upon a Linux kernel feature known as \texttt{seccomp}, which performs process isolation and sandboxing.

				The \emph{MBOX}~\cite{kim} sandboxing project makes use of this solution. MBOX allows an application to run inside a sandboxed file system. The system calls that interact with files are intercepted and modified to use the sandboxed environment instead of the real file system of the host machine. Using \texttt{seccomp-bpf} combined with \texttt{ptrace} greatly reduces the overhead introduced by the interception with \texttt{ptrace} only by intercepting the calls of interest and ignoring the others.

			\subsubsection{DCE library-level interception}
			\label{subsubsec:dcelibintercept}

				The DCE project discussed in section~\ref{subsec:kernellevel} also performs library-level interception. It implements a custom standard C library (\texttt{libc}) which is used by the emulated applications instead of the default \texttt{libc}. As within Microgrid, these new implementations are mainly used to propagate the effects of function calls on the simulated environment instead of the real machine running the emulation.

				The process of rewriting libraries has one major drawback. It is very hard to detect and replace all of the functions that are using some kind of resources inside a program. In addition to the need of finding all the functions to replace, the process of rewriting them is very time consuming. As an example, the DCE project has rewritten over 400 methods of the standard C library, but might need to cover more in the future to be able to run more applications.

				Even though process-level interception gives great control over functions called by the application under study, it could be better to consider the application's source code directly. This, of course, assumes the latter is available to the user willing to emulate the application's behavior.

		\subsection{Source-level interception}
		\label{subsec:sourcelevel}

			One can use what we call \emph{source-level interception} to intercept events happening during program execution, provided that the source code of the application to emulate is available. Source-level interception consists of a set of source code modification targeting key locations of the code. Those key locations could for example be the functions responsible for establishing a network communication or for initiating a data exchange transaction between two processes of the same program. This kind of interception heavily relies on user annotations in the source code. In fact, numerous source-level interception toolkits make use of dedicated compiler \emph{pragmas}, i.e. indications to the compiler on how to treat a given section of the code. Even though inserting those pragmas can be tedious, this approach to runtime event interception allows the user to chose the exact level of detail at which he wants to observe the behavior of the modified application. A common example of source-level interception in the world of HPC simulation is its use to modify certain parts of the source code of MPI applications. These applications use the MPI (\emph{Message Passing Interface}) API to communicate between numerous computation nodes on which they are executing.

			One of the major MPI application simulation toolkits is SST Macro~\cite{adalsteinsson}. SST Macro is an MPI application simulator which makes heavy use of preprocessor directives (i.e. pragmas) to execute an MPI application on a single host while tricking it into believing it is executing on multiple nodes concurrently. It is also able to simulate the behavior of an application using program execution trace replay. However, this trace replay approach has the major drawback of needing real hardware to run the application on at least once to collect the data required to build a meaningful execution trace.

			An alternative to SST Macro is SMPI~\cite{degomme}. SMPI is an MPI application simulation toolkit built on top of SimGrid~\cite{casanova}. It makes use of source-level interception in the form of source code annotation using macros and pragmas to intercept MPI calls. SMPI provides a custom implementation of a large part of the most common MPI functions. Those functions are statically replaced in the simulated application's source code by a dedicated compiler, \texttt{smpicc}. By using custom macros, the user can also chose to remove parts of the original source code to reduce memory consumption during simulation. The removed source code parts account for a fixed amount of simulation time, which is added to the simulated clock each time such a section is encountered.

			Source code modification using macros and/or pragmas can be a good way to intercept events happening during the execution of an application. It allows the user to precisely select the level of granularity at which the observations of the runtime behavior are made. Using this approach, one could change the source code of a cloud application to adapt it to an execution on a single host. This kind of modifications could, for example, take place in the networking and communication interfaces implemented in the original source code.

	\section{Contribution}
	\label{sec:contribution}

		%Contains all the contribution of our Work
		% Modifications of Ceph to study his network reaction.
		%      Disk latency
		%      Network latency

	\section{Evaluation}
	\label{sec:evaluation}

		% An analyse of the work done during this project.
		% Is it concluant, or not, ...

	\section{Conclusion}
	\label{sec:conclusion}

		In this paper, we have established a survey and given a broad overview of numerous software interception techniques. These techniques could be used to emulate a cloud-based application by intercepting every communication event happening during its execution. By using such an approach, one could trick the application into believing it is executing on a real-world cloud infrastructure, whereas it is run on a single host computer.

		The emulation of cloud-based applications on such a small experimenting platform would be of great use to study the behavior of such applications in various execution conditions. It would allow the user to change experimental settings and tweak runtime parameters to precisely determine the evolution of the applications under a number of external conditions.

		In the future, we plan to give a proof of concept of such emulation by interception, using a real-world cloud application. We will focus our attention on Ceph, a popular cloud-based application~\cite{weil} providing storage services that can handle large amounts of data. We will use RSG as an interface to SimGrid, which will be the supporting emulation platform for our experiments.

	\section*{Acknowledgements}

		We would like to thank Martin Quinson\footnotemark and Millian Poquet\footnotemark[\value{footnote}] for their guidance and their precious advice on our work.

		\footnotetext{Myriads team, IRISA, Univ Rennes, France}

	\bibliographystyle{IEEEtran}
	\bibliography{IEEEabrv,biblio}

\end{document}
