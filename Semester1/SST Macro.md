Le concept des interpréteurs de niveau code source est de compiler de manière spécifique du code source, utilisant les directives #pragma.

Le développeur doit se charger de commenter son code avec ces directives pour signaler si certains éléments peuvent ne pas être exécuté, être supprimé, ...

Le travail de préparation est donc une tâche longue et fastidieuse, mais qui permet derrière une exécution adaptée parfaitement aux souhaits de notre étude et qui peux conserver une part non négligeable de la sémantique du programme, car modifiée par un humain, et non automatiquement.


SST Macro est un système développé par "National Technology and Engineering Solutions of Sandia, LLC.", USA

SST Macro est un simulateur d'applications MPI, concurent direct de Simgrid et SMPI.

Son fonctionnement : Simulation par rejeu de traces MPI réelles (ne contient que les Appels MPI, mais reste extensible), ou par exécuion d'un code modifié (avec les directives #pragma)

Principale problématique : Il n'est pas encore possible d'automatiser la mise en place des pragma en dehors des cas les plus simples, ce qui contraint l'utilisateur de SST Macro à modifier/adapter en profondeur tout le code de son logiciel pour pouvoir le simuler, ou à l'exécuter au moins une fois sur une plateforme réelle.

SST Macro fonctionne au niveau du code source, et ne nécessite donc pas d'effectuer d'interception spécifiques, la plupart de celles-ci étant gérées par le compilateur et les directives de compilation #pragma